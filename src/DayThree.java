import helperObjects.ReadDatFile;

import java.util.ArrayList;
import java.util.List;

public class DayThree {
    public static void main(String[] args){
        List<String> treeRows = ReadDatFile.readFile("src/data/dayThree.txt");
        List<String> longTreeRows = buildLongerTreeRows(treeRows);
        System.out.println("There are "+partOne(longTreeRows)+" trees");
        System.out.println("------PART TWO-----");
        System.out.println(partTwo(longTreeRows));

    }

    private static int partOne(List<String> longTreeRows) {
        int moveRight = 3;
        int treeNum = 0;
        int pos = 0;
        for(String s : longTreeRows){
            if(s.charAt(pos) == '#'){
                treeNum++;
            }
            pos+= moveRight;
        }
        return treeNum;
    }

    private static long partTwo(List<String> longTreeRows) {

        return (long)traverseHill(1, longTreeRows, false) *
                traverseHill(3, longTreeRows, false) *
                traverseHill(5, longTreeRows, false) *
                traverseHill(7, longTreeRows, false) *
                traverseHill(1, longTreeRows, true);

    }

    public static int traverseHill(int moveRight, List<String> longTreeRows, boolean downTwo){
        int treeNum = 0;
        int pos = 0;
        if(downTwo){
            for(int i = 0; i <= longTreeRows.size(); i+=2) {
                if (longTreeRows.get(i).charAt(pos) == '#') {
                    treeNum++;
                }
                pos += moveRight;
            }
            return treeNum;
        }
        else {
            for (String s : longTreeRows) {
                if (s.charAt(pos) == '#') {
                    treeNum++;
                }
                pos += moveRight;
            }
            return treeNum;
        }
    }

    private static List<String> buildLongerTreeRows(List<String> treeRows) {
        List<String> longTreeRows = new ArrayList<>();
        for(String s: treeRows){
            StringBuilder builder = new StringBuilder();
            int i = 0;
            while(i < 500){
                builder.append(s);
                i++;
            }
            longTreeRows.add(builder.toString());
        }
        return longTreeRows;
    }
}
