import helperObjects.ReadDatFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DaySix {
    public static void main(String[] args){
        List<String> seatData = ReadDatFile.readFile("src/data/daySix.txt");
        System.out.println("The sum of YES questions is = "+partOne(seatData));
        System.out.println("The number of questions everyone in a group answered YES is = " + partTwo(seatData));
    }

    /**
     * Collects all the people and their letters in to a group Map. Then
     * the group is validated or checked for duplicate letters amongst all
     * people in the group.
     *
     * @param seatData
     * @return
     */
    private static int partTwo(List<String> seatData) {
        Map<Integer, List<Character>> groupMap = new HashMap<>();
        int qSum = 0;
        int person=0;
        for(String s: seatData){
            if(s.isEmpty()){
                qSum += checkGroupForDups(groupMap);
                groupMap.clear();
                person = 0;
            }
            else{
              List<Character> personsYesList = s.chars().mapToObj(c -> (char)c).collect(Collectors.toList());
              groupMap.put(person++, personsYesList);
            }
        }
        return qSum + checkGroupForDups(groupMap);
    }

    /**
     * If the group is only one person, just retern the size of the list. Else add all
     * letters from the first person to a list. Then check every other person for the same
     * letters, if they don't have them. remove it from the original list.
     *
     * @param groupMap
     * @return
     */
    private static int checkGroupForDups(Map<Integer, List<Character>> groupMap) {
        if(groupMap.size() ==1){
            return groupMap.get(0).size();
        }
        List<Character> goodLetters = new ArrayList<>(groupMap.get(0));
        for(int i = 0; i < groupMap.size(); i ++){
            List<Character> person = groupMap.get(i);
            goodLetters.removeIf(c -> !person.contains(c));
        }
        return goodLetters.size();
    }


    /**
     * Iterates over the seatData and moves the "yes" questions (the
     * letters) into a set, then adds them all together.
     *
     * @param seatData
     * @return
     */
    private static int partOne(List<String> seatData) {
        Set<Character> questions = new HashSet<>();
        int qSum = 0;
        for(String s: seatData){
            if(s.isEmpty()){
                qSum+=questions.size();
                questions.clear();
            }
            else{
                for(char c : s.toCharArray()){
                    questions.add(c);
                }
            }
        }
        qSum+=questions.size();
        return qSum;
    }
}
