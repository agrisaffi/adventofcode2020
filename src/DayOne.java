import helperObjects.ReadDatFile;

import java.util.ArrayList;
import java.util.List;

public class DayOne {
    public static void main(String [] args){
        List<String> nums= ReadDatFile.readFile("src/data/dayOne.txt");
        List<Integer> dataNums = new ArrayList<>();
        for(String s: nums){
            dataNums.add(Integer.valueOf(s));
        }
        partOne(dataNums);
        partTwo(dataNums);
    }

    /**
     * Find the numbers that add up to 2020. Then print out the product of the two
     * @param dataNums
     */
    public static void partOne(List<Integer> dataNums){
        for(int i: dataNums){
            if(dataNums.contains(2020 - i)){
                System.out.println(i*(2020-i));
            }
        }
    }

    /**
     * Find what 3 numbers add up to 2020 and then print out the product of the three
     * @param dataNums
     */
    public static void partTwo(List<Integer> dataNums){
        for(int i = 0; i < dataNums.size(); i++){
            for(int j = i; j < dataNums.size(); j++){
                if(i ==j ){continue;}
                int sum = dataNums.get(i) + dataNums.get(j);
                if(dataNums.contains(2020- sum)){
                    int k = 2020-sum;
                    System.out.println(dataNums.get(i) * dataNums.get(j) * k);
                }
            }
        }
    }
}
