package helperObjects;

public class PasswordPolicy {
    public int min;
    public int max;
    public char letter;
    public String password;

    public PasswordPolicy(){}
    public PasswordPolicy(int min, int max, char letter, String password){
        this.min = min;
        this.max = max;
        this.letter = letter;
        this.password = password;
    }

    public int getMin(){
        return min;
    }

    public int getMax(){
        return max;
    }

    public char getLetter() {
        return letter;
    }

    public String getPassword() {
        return password;
    }
}
