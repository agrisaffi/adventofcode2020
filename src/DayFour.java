import helperObjects.ReadDatFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DayFour {
    public static List<String> validFields = new ArrayList<>();

    public static void main(String[] args) {
        //populate valid fields with the right passport info
        validFields.add("byr");
        validFields.add("iyr");
        validFields.add("eyr");
        validFields.add("hgt");
        validFields.add("hcl");
        validFields.add("ecl");
        validFields.add("pid");
        List<String> passFromFile = ReadDatFile.readFile("src/data/dayFour.txt");
        List<String> passAsSingleString = new ArrayList<>();
        List<String> passportInfo = new ArrayList<>();

        //convert each passport information into a single String.
        StringBuilder sb = new StringBuilder();
        for (String passportDatum : passFromFile) {
            if (passportDatum.isEmpty()) {
                passAsSingleString.add(sb.toString());
                sb = new StringBuilder();
            } else {
                sb.append(passportDatum).append(" ");
            }
        }
        passAsSingleString.add(sb.toString());
        //convert each field for a passport into a seperate line. At the end of the person's
        //passport, add an = to signal a new person.
        for(String p : passAsSingleString){
            String[] splity = p.split("\\s");
            Collections.addAll(passportInfo, splity);
            passportInfo.add("=");
        }

        System.out.println("There are "+partOne(passportInfo)+ " valid passports.");
        System.out.println("There are "+partTwo(passportInfo)+ " valid passports. for Part Two");


    }

    /**
     * Populates a SET of passport fields for a person and then
     * checks to see if they are valid.
     *
     * @param passportInfo
     * @return
     */
    private static int partOne(List<String> passportInfo) {
        int validPassports = 0;
        Set<String> passFields = new HashSet<>();
        for (String s : passportInfo) {
            if(s.equals("=")){
                if(validatePassport(passFields)){
                    validPassports++;
                }
                passFields.clear();
            }
            else{
                String[] splity = s.split(":");
                if(splity[0].equals("cid")){
                    continue;
                }
                passFields.add(splity[0]);
            }
        }
        return validPassports;
    }

    /**
     * Creates a SET of passport fields and then validates the information
     * for each field. If valid it is counted.
     *
     * @param passportInfo
     * @return
     */
    private static int partTwo(List<String> passportInfo) {
        int validPassports = 0;
        Set<String> passFields = new HashSet<>();
        for (String s : passportInfo) {
            if(s.equals("=")){
                if(validatePassport(passFields)){
                    validPassports++;
                }
                passFields.clear();
            }
            else{
                String[] splity = s.split(":");
                if(isValid(splity)) {
                    passFields.add(splity[0]);
                }
            }
        }
        return validPassports;
    }

    /**
     * Validates a specific field based off rules set on
     * AdventOfCode.
     *
     * @param splity
     * @return
     */
    private static boolean isValid(String[] splity) {
        switch (splity[0]){
            case "byr":
                if(splity[1].length() != 4){
                    return false;
                }
                int bYear = Integer.parseInt(splity[1]);
                return bYear >= 1920 && bYear <= 2020;

            case "iyr":
                if(splity[1].length() != 4){
                    return false;
                }
                int iYear = Integer.parseInt(splity[1]);
                return iYear >= 2010 && iYear <= 2020;

            case "eyr":
                if(splity[1].length() != 4){
                    return false;
                }
                int eYear = Integer.parseInt(splity[1]);
                return eYear >= 2020 && eYear <= 2030;

            case "hgt":
                if(!splity[1].contains("i") && !splity[1].contains("c")){
                    return false;
                }
                char[] heightLetters = splity[1].toCharArray();
                StringBuilder stToNum = new StringBuilder();
                for(char c : heightLetters){
                    if(c != 'i' && c != 'c'){
                        stToNum.append(c);
                    }
                    else{
                        int num = Integer.parseInt(stToNum.toString());
                        if(c == 'i' && num >=59 && num <= 76){
                            return true;
                        }
                        return c == 'c' && num >= 150 && num <= 193;
                    }
                }
                break;
            case "hcl":
                char[] hNums = splity[1].toCharArray();
                if (hNums[0] != '#') {
                    return false;
                }
                if(hNums.length-1 != 6){
                    return false;
                }
                for(char c : hNums){
                    if(c != '#'){
                        if (((int) c < 48 || (int) c > 57) && ((int) c < 97 || (int) c > 102)) {
                            return false;
                        }
                    }
                }
                return true;
            case "ecl":
                String color = splity[1];
                return color.equals("amb") ||
                        color.equals("blu") ||
                        color.equals("brn") ||
                        color.equals("gry") ||
                        color.equals("grn") ||
                        color.equals("hzl") ||
                        color.equals("oth");
            case "pid":
                return splity[1].length() == 9;
            default:
                break;
        }
        return true;
    }

    /**
     * Checks if the passed in fields are the same as the valid ones.
     *
     * @param passFields
     * @return
     */
    private static boolean validatePassport(Set<String> passFields) {
        for(String s: validFields){
            if(!passFields.contains(s)){
                return false;
            }
        }
        return true;
    }
}

