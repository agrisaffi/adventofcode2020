import helperObjects.PasswordPolicy;
import helperObjects.ReadDatFile;

import java.util.ArrayList;
import java.util.List;

public class DayTwo {
    public static void main(String [] args) {
        List<String> passwords = ReadDatFile.readFile("src/data/dayTwo.txt");
        List<PasswordPolicy> policyChecker = new ArrayList<>();
        //Parse data into a POJO
        for(String s : passwords){
            String[] colonSplit = s.split(":");
            String pass = colonSplit[1];
            String[] hypSplit = colonSplit[0].split("-");
            int min = Integer.parseInt(hypSplit[0]);
            String[] spaceSplit = hypSplit[1].split(" ");
            int max = Integer.parseInt(spaceSplit[0]);
            PasswordPolicy temp = new PasswordPolicy(min, max, spaceSplit[1].charAt(0),pass);
            policyChecker.add(temp);
        }
        System.out.println("There are "+partOne(policyChecker) + " valid passwords for part one");
        System.out.println("There are "+partTwo(policyChecker) + " valid passwords for part two");

    }

    /**
     * Checks to see if the password is valid by:
     *
     * the password contains the correct letter between the max and min
     * amount of times
     *
     * @param policies
     * @return
     */
    public static int partOne(List<PasswordPolicy> policies){
        int validPasswords = 0;
        for(PasswordPolicy p : policies){
            int count =0;
            char[] passLetters = p.getPassword().toCharArray();
            for(char c : passLetters){
                if(c == p.getLetter()){
                    count++;
                }
            }
            if(count <= p.getMax() && count >= p.getMin()){
                validPasswords++;
            }
        }
        return validPasswords;
    }

    /**
     * Checks to see if the password contians the specified letter only ONCE
     * in the min/max location.
     *
     * @param policyChecker
     * @return
     */
    private static int partTwo(List<PasswordPolicy> policyChecker) {
        int validPasswords =0;
        for(PasswordPolicy p : policyChecker){
            int pos1 = p.getMin();
            int pos2 = p.getMax();
            if(p.getPassword().charAt(pos1) == p.getLetter() &&
                    p.getPassword().charAt(pos2) != p.getLetter()){
                validPasswords++;
            }
            if(p.getPassword().charAt(pos1) != p.getLetter() &&
                    p.getPassword().charAt(pos2) == p.getLetter()){
                validPasswords++;
            }
        }
        return validPasswords;
    }

}
