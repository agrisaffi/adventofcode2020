import helperObjects.ReadDatFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DayFive {
    public static void main(String[] args){
        List<String> seatData = ReadDatFile.readFile("src/data/dayFive.txt");
        List<Integer> seatValues = partOne(seatData);
        System.out.println("Part one = " +seatValues.get(seatValues.size()-1));
        System.out.println("Your seat number is  = "+partTwo(seatValues));
    }

    /**
     * Finds the seat number that is not in the seatValues list
     *
     * @param seatValues
     * @return
     */
    private static int partTwo(List<Integer> seatValues) {
        int seatNum = seatValues.get(0);
        for(int i : seatValues){
            if(i != seatNum){
                return seatNum;
            }
            seatNum++;
        }
        return 0;
    }

    /**
     * Calculates all the seat numbers for all boarding passengers
     *
     * @param seatData
     * @return
     */
    private static List<Integer> partOne(List<String> seatData) {
        List<Integer> seatValues = new ArrayList<>();
        for(String s: seatData){

            int row = calculateRow(s.substring(0, 7));
            int col = calculateCol(s.substring(7));
            seatValues.add((row*8)+col);
        }
        Collections.sort(seatValues);
        return seatValues;
    }


    /**
     * Converts the row to a binary number, then returns the
     * decimal value.
     *
     * @param rows
     * @return
     */
    private static int calculateRow(String rows) {

        StringBuilder binaryString = new StringBuilder();
        for(char c : rows.toCharArray()){
            if(c == 'F'){
                binaryString.append(0);
            }
            else{
                binaryString.append(1);
            }
        }
        return Integer.parseInt(binaryString.toString(),2);
    }

    /**
     * Converts the col to a binary number, then returns the
     * decimal value.
     *
     * @param cols
     * @return
     */
    private static int calculateCol(String cols) {
        StringBuilder binaryString = new StringBuilder();
        for(char c : cols.toCharArray()){
            if(c == 'R'){
                binaryString.append(1);
            }
            else{
                binaryString.append(0);
            }
        }
        return Integer.parseInt(binaryString.toString(),2);
    }

}
